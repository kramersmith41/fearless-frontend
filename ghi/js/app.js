function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="col">
            <div class="card" style="box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.5); margin: 20px;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">${name}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                  <p class="card-text">${description}</p>
                </div>
            <div class="card-footer text-muted">
                ${start} - ${end}
            </div>
        </div>
    `;
  }

var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
    alertPlaceholder.append(wrapper)
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        alert('Error: The response request was not ok', 'success');
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const start = new Date(details.conference.starts).toLocaleDateString();
            const end = new Date(details.conference.ends).toLocaleDateString();
            const html = createCard(title, description, pictureUrl, start, end, location);
            const row = document.querySelector('.row');
            row.innerHTML += html
          }
        }
      }
    } catch (e) {
        console.error(e);
        alert(`Error: ${e}`, 'success')
    }
  });