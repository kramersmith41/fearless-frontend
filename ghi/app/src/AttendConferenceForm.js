import React from 'react';


class AttendConferenceForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        conferences: [],
        name: '',
        email: ''
      };
      this.handleConferenceChange = this.handleConferenceChange.bind(this);
      this.handleemailChange = this.handleemailChange.bind(this);
      this.handlenameChange = this.handlenameChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        delete data.conferences;
        console.log(data);
        const attendeeUrl = `http://localhost:8001${data.conference}attendees/`;
        delete data.conference
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
          const newAttendee = await response.json();
          console.log(newAttendee);
          const cleared = {
            conference: "",
            name: '',
            email: '',
          };
          this.setState(cleared);
        } else {
            console.error(response)
        }
      }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
    }

    handleemailChange(event) {
        const value = event.target.value;
        this.setState({email: value})
    }

    handlenameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    async componentDidMount() {
        const spinnerTag = document.getElementById('loading-conference-spinner');
        const conferenceTag = document.getElementById('conference')
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences})
            if (this.state.conferences) {
                spinnerTag.classList.add('d-none');
                conferenceTag.classList.remove('d-none')
            }
        }
    }

    render() {
      return (
        <div className="my-5">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={this.handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleConferenceChange} name="conference" id="conference" className="form-select d-none" required>
                    <option value="">Choose a conference</option>
                    {this.state.conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={this.handlenameChange} required placeholder="Your full name"
                      type="text" id="name" name="name" className="form-control" value={this.state.name} />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleemailChange} required placeholder="Your email address"
                      type="email" id="email" name="email" className="form-control" value={this.state.email} />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      );
    }
  }

export default AttendConferenceForm;