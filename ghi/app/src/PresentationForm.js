import React from "react";

export default class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conferences: []
        }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCompanyChange = this.handleCompanyChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
    this.handleConferenceChange = this.handleConferenceChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        delete data.conferences;
        console.log(data);
        const presentationUrl = `http://localhost:8000${data.conference}presentations/`;
        delete data.conference
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            const cleared = {
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: ''
            };
            this.setState(cleared);
        } else {
            console.error(response)
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({presenter_name: value});
    }

    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({presenter_email: value});
    }

    handleCompanyChange(event) {
        const value = event.target.value;
        this.setState({company_name: value});
    }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value});
    }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({synopsis: value});
    }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({conferences: data.conferences});
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form id="create-presentation-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} placeholder="Presenter name" required
                        type="text" name="presenter_name" id="presenter_name"
                        className="form-control" value={this.state.presenter_name} />
                        <label htmlFor="name">Presenter name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEmailChange} placeholder="Presenter email"
                        required type="text" name="presenter_email" id="presenter_email"
                        className="form-control" value={this.state.presenter_email} />
                        <label htmlFor="presenter_email">Presenter email</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleCompanyChange} placeholder="company_name"
                        required type="text" name="company_name" id="company_name"
                        className="form-control" value={this.state.company_name} />
                        <label htmlFor="company_name">Company name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleTitleChange} placeholder="title"
                        required type="text" name="title" id="title"
                        className="form-control" value={this.state.title} />
                        <label htmlFor="title">Title</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="synopsis" className="form-label">Synopsis</label>
                        <textarea onChange={this.handleSynopsisChange} required id="synopsis"
                        name="synopsis" className="form-control" rows="3" value={this.state.synopsis}></textarea>
                    </div>
                    <div className="mb-3">
                            <select onChange={this.handleConferenceChange} required id="conference" name="conference" className="form-select">
                                <option value="">Choose a Conference</option>
                                {this.state.conferences.map(conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    <div></div>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                        Congratulations! New Presentation Created!
                    </div>
                </div>
                </div>
            </div>
        );
    }
}